import { Component } from '@angular/core';
import { BooksService } from './generated/api/v1/api/books.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'open-api-example';
  $books = this.booksService.getBooks();

  constructor(private readonly booksService: BooksService) {}
}
