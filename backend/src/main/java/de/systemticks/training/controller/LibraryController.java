package de.systemticks.training.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.systemticks.training.api.BooksApi;
import de.systemticks.training.api.model.Book;


@RequestMapping("/api")
@RestController
public class LibraryController implements BooksApi {

    @Override
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<Book>> getBooks() {
        // TODO Auto-generated method stub
        List<Book> books = new ArrayList<>();
        Book book = new Book();
        book.setDate(LocalDate.now());
        book.setDescription("An article description");
        book.setId(UUID.randomUUID());
        book.setImageUrl("https://images.manning.com/360/480/resize/book/6/4771315-b5e7-4c00-b13b-c94f37d1d624/Ponelat-Designing-MEAP-HI.png");
        book.setTitle("Designing APIs with Swagger and OpenAPI");
        books.add(book);

        return new ResponseEntity<>(books, HttpStatus.OK);
    }
    
}
